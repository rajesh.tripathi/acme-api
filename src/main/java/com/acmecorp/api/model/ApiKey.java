package com.acmecorp.api.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "api_keys")
public class ApiKey {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, unique = true)
    private String apiKey;
    @Column(nullable = false)
    private String role;
}
