package com.acmecorp.api.model;

import lombok.Data;

import javax.persistence.*;
@Data
@Entity
@Table(name = "contact_info")
public class ContactInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String email;
    @Column(nullable = false)
    private String phone;
}

