package com.acmecorp.api.model;

import lombok.Data;

import javax.persistence.*;
@Data
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String orderNumber;

    @Column(nullable = false)
    private String orderDetails;

    @Column(nullable = false)
    private Long customerId;
}


