package com.acmecorp.api.service;

import com.acmecorp.api.dto.OrderDTO;
import com.acmecorp.api.model.Order;
import com.acmecorp.api.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    public Order getOderById(Long id){

        return orderRepository.findById(id).orElse(null);
    }

    public OrderDTO createOrder(OrderDTO orderDTO){
        Order order = new Order();
        order.setOrderNumber(orderDTO.getOrderNumber());
        order.setOrderDetails(orderDTO.getOrderDetails());
        order.setCustomerId(orderDTO.getCustomerId());
        orderRepository.save(order);
        return orderDTO;
    }

    public List<Order> getAllOrder(){
        return orderRepository.findAll();
    }
}