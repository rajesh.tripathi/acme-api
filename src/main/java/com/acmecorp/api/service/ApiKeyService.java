package com.acmecorp.api.service;

import com.acmecorp.api.model.ApiKey;
import com.acmecorp.api.repository.ApiKeyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class ApiKeyService {
    @Autowired
    private ApiKeyRepository apiKeyRepository;

    public boolean isValidApiKey(String apiKey) {
        return apiKeyRepository.findByApiKey(apiKey).isPresent();
    }

    public String getRole(String apiKey) {
        return apiKeyRepository.findByApiKey(apiKey)
                .map(ApiKey::getRole)
                .orElse(null);
    }

    public ApiKey createApiKey(ApiKey apiKey){
        return apiKeyRepository.save(apiKey);
    }

    public ApiKey getApiKey(String apiKey){
        return apiKeyRepository.findByApiKey(apiKey).orElse(null);
    }
}
