package com.acmecorp.api.service;

import com.acmecorp.api.model.ContactInfo;
import com.acmecorp.api.repository.ContactInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ContactInfoService {

    @Autowired
    private ContactInfoRepository contactInfoRepository;
    public ContactInfo getContactInfoById(Long id){
        return contactInfoRepository.findById(id).orElse(null);
    }

    public ContactInfo addContactInfo(ContactInfo contactInfo){
        return contactInfoRepository.save(contactInfo);
    }

    public List<ContactInfo> getAllContactInfo(){
        return contactInfoRepository.findAll();
    }
}
