package com.acmecorp.api.service;

import com.acmecorp.api.dto.ContactInfoDTO;
import com.acmecorp.api.dto.CustomerDTO;
import com.acmecorp.api.model.ContactInfo;
import com.acmecorp.api.model.Customer;
import com.acmecorp.api.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private ContactInfoService contactInfoService;

    public CustomerDTO getCustomerById(Long id) {

        Customer customer = customerRepository.findById(id).orElse(null);
        ContactInfo contactInfo = contactInfoService.getContactInfoById(customer.getContactId());
        ContactInfoDTO contactInfoDTO = new ContactInfoDTO();
        contactInfoDTO.setPhone(contactInfo.getPhone());
        contactInfoDTO.setEmail(contactInfo.getEmail());
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setCustomerName(customer.getName());
        customerDTO.setContactInfo(contactInfoDTO);
        return customerDTO;
    }

    public CustomerDTO addCustomer(CustomerDTO customerDTO){

        ContactInfo contactInfo = new ContactInfo();
        contactInfo.setEmail(customerDTO.getContactInfo().getEmail());
        contactInfo.setPhone(customerDTO.getContactInfo().getPhone());
        ContactInfo savedContact = contactInfoService.addContactInfo(contactInfo);
        Customer customer = new Customer();
        customer.setContactId(savedContact.getId());
        customer.setName(customerDTO.getCustomerName());
        customerRepository.save(customer);
        return customerDTO;
    }

    public List<CustomerDTO> getAllCustomer(){
        return customerRepository.findAll()
                .stream()
                .map(c->getCustomers(c))
                .collect(Collectors.toList());

    }

    public CustomerDTO getCustomers(Customer c) {
        CustomerDTO customerDTO = new CustomerDTO();
        ContactInfo contactInfo = contactInfoService.getContactInfoById(c.getContactId());
        customerDTO.setCustomerName(c.getName());
        ContactInfoDTO contactInfoDTO = new ContactInfoDTO();
        contactInfoDTO.setPhone(contactInfo.getPhone());
        contactInfoDTO.setEmail(contactInfo.getEmail());
        customerDTO.setContactInfo(contactInfoDTO);
        return customerDTO;
    }
}