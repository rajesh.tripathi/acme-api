package com.acmecorp.api.dto;

import lombok.Data;

@Data
public class ContactInfoDTO {
    private String email;
    private String phone;
}
