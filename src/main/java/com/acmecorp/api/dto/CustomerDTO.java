package com.acmecorp.api.dto;

import lombok.Data;

@Data
public class CustomerDTO {
    private String customerName;
    private ContactInfoDTO contactInfo;
}
