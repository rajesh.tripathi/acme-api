package com.acmecorp.api.dto;

import lombok.Data;

@Data
public class OrderDTO {
    private String orderNumber;
    private String orderDetails;
    private Long customerId;
}
