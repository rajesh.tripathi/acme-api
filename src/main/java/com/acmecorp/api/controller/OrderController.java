package com.acmecorp.api.controller;

import com.acmecorp.api.dto.OrderDTO;
import com.acmecorp.api.model.Order;
import com.acmecorp.api.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/orders")
public class OrderController {
    @Autowired
    private OrderService orderService;
    @PostMapping("/")
    public ResponseEntity<OrderDTO> createOrder(@RequestBody OrderDTO orderData){
        //To do: // validation for customer can be done, whether customerId is valid or not
        OrderDTO order = orderService.createOrder(orderData);
        if (order != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(order);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    @GetMapping("/{id}")
    public ResponseEntity<Order> getOderById(@PathVariable Long id) {
        Order order = orderService.getOderById(id);
        if (order != null) {
            return ResponseEntity.ok(order);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
        }
    }

    @GetMapping("/")
    public ResponseEntity<List<Order>> getAllOrder(){
        List<Order> orderList = orderService.getAllOrder();
        if (!CollectionUtils.isEmpty(orderList)) {
            return ResponseEntity.ok(orderList);
        } else {
            return ResponseEntity.ok().build();
        }
    }
}