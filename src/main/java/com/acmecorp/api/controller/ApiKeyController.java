package com.acmecorp.api.controller;


import com.acmecorp.api.CustomeException;
import com.acmecorp.api.model.ApiKey;
import com.acmecorp.api.service.ApiKeyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/apiKeys")
public class ApiKeyController {
    Logger logger = LoggerFactory.getLogger(ApiKeyController.class);
    @Autowired
    private ApiKeyService apiKeyService;

    @PostMapping("/")
    public ApiKey createApiKey(@RequestBody ApiKey apiKey){
        try{
            return apiKeyService.createApiKey(apiKey);
        }catch (Exception e){
            logger.error("Error..."+e.getLocalizedMessage());
            throw new CustomeException();
        }
    }

    @GetMapping("/{apiKey}")
    public ApiKey getByApiKey(@PathVariable String apiKey){
        try{
            return apiKeyService.getApiKey(apiKey);
        }catch (Exception e){
            logger.error("Error..."+e.getLocalizedMessage());
            throw new CustomeException();
        }
    }

    @DeleteMapping("/{apiKey}")
    public void removeApiKey(@PathVariable String apiKey){
    }
}
