package com.acmecorp.api.controller;

import com.acmecorp.api.CustomeException;
import com.acmecorp.api.dto.CustomerDTO;
import com.acmecorp.api.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/customers")
public class CustomerController {
    Logger logger = LoggerFactory.getLogger(ApiKeyController.class);
    @Autowired
    private CustomerService customerService;
    @PostMapping("/")
    public ResponseEntity<CustomerDTO> addCustomer(@RequestBody CustomerDTO customerDTO){
        try{
            CustomerDTO customer = customerService.addCustomer(customerDTO);
            if (customer != null) {
                return ResponseEntity.status(HttpStatus.CREATED).body(customer);
            } else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception e){
            logger.error("Error..."+e.getLocalizedMessage());
            throw new CustomeException();
        }
 }
    @GetMapping("/{id}")
    public ResponseEntity<CustomerDTO> getCustomerById(@PathVariable Long id) {
        try{
            CustomerDTO customer = customerService.getCustomerById(id);
            if (customer != null) {
                return ResponseEntity.ok(customer);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
            }
        }catch (Exception e){
            logger.error("Error..."+e.getLocalizedMessage());
            throw new CustomeException();
        }
    }

    @GetMapping("/")
    public ResponseEntity<List<CustomerDTO>> getAllCustomer(){
        List<CustomerDTO> customerList = customerService.getAllCustomer();
        if (!CollectionUtils.isEmpty(customerList)) {
            return ResponseEntity.ok(customerList);
        } else {
            return ResponseEntity.ok().build();
        }
    }

    @GetMapping("/{customerName}")
    public ResponseEntity<CustomerDTO> getAllCustomer(@PathVariable String customerName){
        return null;
    }

    @DeleteMapping("/{customerName}")
    public ResponseEntity<Void> removeCustomer(@PathVariable String customerName){
        return null;
    }
}